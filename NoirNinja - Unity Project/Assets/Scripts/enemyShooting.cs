﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyShooting : MonoBehaviour
{
    // Shooting
    public GameObject bullet;
    public float fireRate;
    private float nextFire;
    Vector2 bulletPos;
    public Transform firePoint;

    // Audio
    public AudioSource shootSound;

    void Start()
    {
        shootSound = GetComponent<AudioSource>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Fire();
        }
    }

    void Fire()
    {
        bulletPos = firePoint.position; // Set the bullets position to the empty game object
        Instantiate(bullet, bulletPos, Quaternion.identity);
        shootSound.Play();
    }
}