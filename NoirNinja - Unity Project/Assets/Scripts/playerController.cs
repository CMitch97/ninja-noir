﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class playerController : MonoBehaviour
{
    // Movement and Jumping
    private Rigidbody2D rb;
    public float speed;
    public float jumpForce;
    private float moveInput;

    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    private float jumpTimeCounter;
    public float jumpTime;
    bool isJumping;

    // Dashing
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;
    public Text dashText; // Shows the amount of dashes left on screen
    bool isDashing;

    // Dash Cooldown
    public int dashCounter;
    private int remainingDashes;
    public float dashTimeToRecharge; // The total time it takes to recharge a dash charge
    private float dashChargeTime; // How long we currently have until we recharge a dash charge

    // Health Variables
    public int maxHealth;
    private int remainingHealth;
    public Text healthText;

    // Shooting
    public GameObject bulletRight, bulletLeft;
    Vector2 bulletPos;
    public float fireRate = 0.5f;
    float nextFire = 0.0f;
    public Transform firePoint;
    bool isShooting;

    // Animations
    private Animator Anim;

    // Scoring
    public scoreController playerScore;

    void Start()
    {
        // Sets the rb variable to the player objects rigidBody2D component
        rb = GetComponent<Rigidbody2D>();

        dashTime = startDashTime;

        remainingDashes = dashCounter;

        Anim = GetComponent<Animator>();

        // Display the amount of dashes the player has at the start of the level
        dashText.text = "Energy: " + remainingDashes + "/3";

        // Set the players health to 100 at the start of a level
        remainingHealth = maxHealth;

        // Display the players starting Health
        healthText.text = "Health: " + remainingHealth;

        // Disable death text
        GameObject.Find("playerCanvas").GetComponent<Canvas>().enabled = false;
    }

    void FixedUpdate()
    {
        // Finds out if the player presses either move button then moves them in the direction they have pressed
        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        // Test to see if player is moving
        // If so, play the moving animation
        if (moveInput == 0)
        {
            Anim.SetBool("isRunning", false);
        }
        else
        {
            Anim.SetBool("isRunning", true);
        }

        // Dashing
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            direction = 1; // When the player moves right, the direction variable is equal to 1
            transform.localScale = new Vector2(2f, 2f); // Flip player Sprite
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            direction = -1; // When the player moves left, the direction variable is equal to -1
            transform.localScale = new Vector2(-2f, 2f); // Flip player Sprite
        }

        // Record that the user started a dash (setting dashTime to startDashTime) - this is an if for GetKeyDown (KeyCode.LeftShift) and if they have any dashes left (remaining dashes = to 0)
        if (Input.GetButtonDown("Dash") && remainingDashes > 0)
        {
            dashTime = startDashTime;
            remainingDashes -= 1; // removes 1 from the player's remaining dashes
            dashText.text = "Energy: " + remainingDashes + "/3"; // Change Dash UI
            isDashing = true;
        }
        else
        {
            isDashing = false;
        }

        // Play the dash animation if isDashing = true
        if (isDashing == true)
        {
            Anim.SetBool("isDashing", true);
        }
        else if (isDashing == false)
        {
            Anim.SetBool("isDashing", false);
        }

        // Check if the user is currently dashing (check if dashTime > 0) 
        if (dashTime > 0)
        {
            // Dashes in the direction that is stored in the direction variables
            // If so, reduce dashTime by current frame time
            dashTime -= Time.deltaTime;
            isDashing = true; // Set is dashing to true

            // Also if so, actually set the velocity for the player's dash (based on the previously recorded direction)
            if (direction == 1)
            {
                rb.velocity = Vector2.right * dashSpeed;
            }
            else if (direction == -1)
            {
                rb.velocity = Vector2.left * dashSpeed;
            }
        }

        // Regain dash charges if not at max
        if (remainingDashes < dashCounter)
        {
            // if we havent started recharging, do so
            if (dashChargeTime <= 0)
            {
                dashChargeTime = dashTimeToRecharge;
            }

            // remove the last frame's time from our dash charge time
            dashChargeTime -= Time.deltaTime;

            // If we have charged long enough, add a charge and stop recharging
            if (dashChargeTime <= 0)
            {
                remainingDashes += 1;
                dashText.text = "Energy: " + remainingDashes + "/3"; // Change Dash UI
            }
        }
    }

    void Update()
    {
        // Checks to see if a circle below the players feet is colliding with the ground
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        // Jumps if player is colliding with ground
        if (isGrounded == true && Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            jumpTimeCounter = jumpTime; // Reset the counter to the jump time variable which is set in the editor
            rb.velocity = Vector2.up * jumpForce;
            Anim.SetBool("isJumping", true); // Change to the players jump animation
        }

        // Allow the player to jump higher the longer they hold the jump button
        if (Input.GetButton("Jump") && isJumping == true)
        {
            if (jumpTimeCounter > 0)
            {
                rb.velocity = Vector2.up * jumpForce; // Jump if the counter is greater than 0
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJumping = false; // If counter is less than 0 then the player cannot jump
            }
        }
        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false; // Don't jump when the player isn't holding the jump button
        }

        // Play jumping animation if is Jumping is true
        if (isJumping == false)
        {
            Anim.SetBool("isJumping", false);
        }
        else if (isJumping == true)
        {
            Anim.SetBool("isJumping", true);
        }

        // Shooting
        // When the player presses the fire button, shoot once
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            isShooting = true;
            Fire();
        }
        else
        {
            isShooting = false;
        }

        // Play throwing animation if isShooting is true
        if (isShooting == true)
        {
            Anim.SetBool("isThrowing", true);
        }
        else if (isShooting == false)
        {
            Anim.SetBool("isThrowing", false);
        }
    }

    void Fire()
    {
        bulletPos = firePoint.position; // Set the bullets position to the empty game object
        if (direction == 1)
        {
            Instantiate(bulletRight, bulletPos, Quaternion.identity); // Fire right if the player is facing that way
        }
        else if (direction == -1)
        {
            Instantiate(bulletLeft, bulletPos, Quaternion.identity); // Fire left if the player is facing that way
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullets"))
        {
            remainingHealth -= 20;
            Destroy(collision.gameObject);
            healthText.text = "Health: " + remainingHealth;
            playerScore.AddScore(-50);
        }

        if (remainingHealth <= 1)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().isKinematic = true;
            GameObject.Find("playerCanvas").GetComponent<Canvas>().enabled = true; // Enable death text
            enabled = false;
        }

        if (collision.gameObject.CompareTag("Death"))
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Rigidbody2D>().isKinematic = true;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Collider2D>().enabled = false;
            GameObject.Find("playerCanvas").GetComponent<Canvas>().enabled = true; // Enable death text
            enabled = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Death"))
        {
            Destroy(this.gameObject);
        }
    }
}

