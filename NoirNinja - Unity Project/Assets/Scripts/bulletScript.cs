﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public float velX = 5f;
    private float velY = 0f;
    Rigidbody2D rb;

    // Collision
    public GameObject groundCollision;

    void Start()
    {
        rb = GetComponent<Rigidbody2D> ();
    }

    void Update()
    {
        rb.velocity = new Vector2 (velX, velY);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            //Debug.Log("Collision Detected with correct tag!");
            Destroy(this.gameObject);
        }
    }
}
