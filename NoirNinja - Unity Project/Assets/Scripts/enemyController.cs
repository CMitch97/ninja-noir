﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour
{

    // Health
    public int enemyHealth;
    private int remainingEnemyHealth;

    // Scoring
    public scoreController enemyScore;

    // Audio
    public AudioSource hitSound;

    void Start()
    {
        remainingEnemyHealth = enemyHealth;

        hitSound = GetComponent<AudioSource> ();
    }

    
    void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectiles"))
        {
            remainingEnemyHealth -= 50;
            Destroy(collision.gameObject);

            hitSound.Play();
        }

        if (remainingEnemyHealth <= 1)
        {
            hitSound.Play();
            Destroy(gameObject);
            enemyScore.AddScore(100);
        }
    }
}
