﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scoreController : MonoBehaviour
{
    public int startScore;
    int currentScore;
    public Text scoreText;
    public Text endText;

    void Start()
    {
        // Checks to see what scene the player is currently on and stores it in a variable
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        // If the scene is on the first level, the score will be reset to 0 and the saved data will be deleted
        if (sceneName == "Scene1")
        {
            currentScore = startScore;
            scoreText.text = "Score: " + currentScore;
            PlayerPrefs.DeleteAll();
        }
        else
        {
            // Carry score between scenes if the scene is not called Scene1
            PlayerPrefs.GetInt("playerScore");
            currentScore = PlayerPrefs.GetInt("playerScore");
        }

        // Display the players score on the end screen
        if (sceneName == "EndScene")
        {
            endText.text = "Final Score: " + PlayerPrefs.GetInt("playerScore");
        }

    }

    public void AddScore(int scoreToAdd)
    {
        currentScore += scoreToAdd;
    }

    void Update()
    {
        if (scoreText != null)
        {
            scoreText.text = "Score: " + currentScore;
        }

        PlayerPrefs.SetInt("playerScore", currentScore);
    }
}
